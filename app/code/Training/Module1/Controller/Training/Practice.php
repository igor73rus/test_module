<?php
declare(strict_types=1);

namespace Training\Module1\Controller\Training;

use \Magento\Framework\App\Action\HttpGetActionInterface;
use \Magento\Framework\App\Action\Action;

class Practice extends Action implements HttpGetActionInterface
{
    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /**
     * Module page constrtuctor
    */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $is_rewrite = $this->getRequest()->getParam('is_rewrite');
        $resultPage = $this->resultPageFactory->create();

        if ($is_rewrite == 'yes') {
            $resultPage->getConfig()->getTitle()->set('This is rewrite');
        }

        return $resultPage;
    }
}