<?php
declare(strict_types=1);

namespace Training\Module1\Controller\Product;

use \Magento\Framework\App\Action\HttpGetActionInterface;
use \Magento\Framework\App\Action\Action;

class Test extends Action implements HttpGetActionInterface
{
    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultJsonFactory;

    /**
     * Module page constrtuctor
    */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        return $this->resultJsonFactory->create()->setData(["message" => "Test"]);
    }
}
