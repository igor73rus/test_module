<?php

declare(strict_types=1);

namespace Training\Module1\Plugin;

use \Magento\Catalog\Model\Product;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ChangeProductPrice
{
    /**
     * Configuration paths for Module1 settings
    */
    const IS_ENABLED_EXTRA_PRICE = 'training_settings/training_inputs/enable_extra_price';

    const EXTRA_PRICE_VALUE = 'training_settings/training_inputs/extra_price_value';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig) {
        $this->scopeConfig = $scopeConfig;
    }
    /**
     * @param  \Magento\Catalog\Model\Product $product
     * @param  int $price
     * @return int
     */
    public function afterGetPrice(Product $product, $price)
    {
        if ($this->scopeConfig->getValue(self::IS_ENABLED_EXTRA_PRICE)) {
            $coeff = $this->scopeConfig->getValue(self::EXTRA_PRICE_VALUE);
            $price = $price + $coeff;
        }

        return $price;
    }
}