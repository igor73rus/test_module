<?php

namespace Training\Module1\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LogLevel;

class TrainingModule1Observer implements ObserverInterface
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getData('request');
        $url = $request->getPathInfo();
        $this->logger->log(LogLevel::INFO, sprintf('URL in request is "%s"', $url));
    }
}
