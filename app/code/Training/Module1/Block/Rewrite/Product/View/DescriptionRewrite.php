<?php

namespace Training\Module1\Block\Rewrite\Product\View;

use Magento\Catalog\Block\Product\View\Description;
use Magento\Framework\View\Element\Template\Context;

/**
 * Rewrite product Description class
 */
class DescriptionRewrite extends Description
{
    /**
     * Constructor for rewrited description
     *
     * @param Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(Context $context, \Magento\Framework\Registry $registry, array $data = [])
    {
        parent::__construct($context, $registry, $data);
    }

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Product
     */
    protected function _beforeToHtml()
    {
        return $this->getProduct()->setData('description', 'We rewrite this description');
    }

    /**
     * Gets Module name
     *
     * @return string
     */
    public function getModuleName()
    {
        return 'Magento_Catalog';
    }
}