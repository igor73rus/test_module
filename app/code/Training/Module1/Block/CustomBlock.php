<?php

namespace Training\Module1\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Element\AbstractBlock;

class CustomBlock extends AbstractBlock
{
    /**
     * Block constructor
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    protected function _toHtml()
    {
        return 'Custom block for fourth lesson';
    }
}
