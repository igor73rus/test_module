<?php
namespace Training\Module2\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddTestAttribute implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        EavConfig $eavConfig
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->eavConfig = $eavConfig;
    }

     /**
      * {@inheritdoc}
      */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute(Product::ENTITY, 'test_search_words', [
            'type' => 'text',
            'label' => 'Search words',
            'input' => 'text',
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'visible_on_front' => true,
        ]);

        $eavSetup->addAttribute(Product::ENTITY, 'test_multiple', [
            'type' => 'varchar',
            'backend' => ArrayBackend::class,
            'frontend' => 'Training\Module2\Model\Frontend\AttributeStyle',
            'label' => 'Test multiple',
            'input' => 'multiselect',
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'visible_on_front' => true,
            'option' => [
                'values' => [
                    'Left',
                    'Right',
                    'Up',
                    'Down',
                    'All'
                ]
            ]
        ]);

        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $customerSetup->addAttribute(
            Customer::ENTITY,
            'test_priority', [
                'label' => 'Priority',
                'type' => 'int',
                'input' => 'select',
                'system' => false,
                'required' => false,
                'visible' => true,
                'backend' => ArrayBackend::class,
                'source' => 'Training\Module2\Model\Config\Customer\Priority',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'sort_order' => 1000,
                'position' => 1000,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
    * {@inheritdoc}
    */
    public static function getVersion()
    {
        return '2.0.0';
    }
}