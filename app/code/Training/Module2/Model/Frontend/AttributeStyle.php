<?php

namespace Training\Module2\Model\Frontend;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;
use Magento\Framework\DataObject;


class AttributeStyle extends AbstractFrontend
{
    private $_value = '';

    public function getValue(DataObject $object)
    {
        $this->getAttribute()->setData(Attribute::IS_HTML_ALLOWED_ON_FRONT, 1);

        $values = explode(', ', parent::getValue($object));

        foreach ($values as $value) {
            $this->_value .= '<li>' . $value . '</li>';
        }

        return '<ul>' . $this->_value . '</ul>';
    }
}