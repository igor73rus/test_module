<?php

namespace Training\Question\Controller\Question;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Training\Question\Model\QuestionFactory;

class FormPost extends Action implements HttpPostActionInterface
{
    protected $resultPageFactory;
    protected $questionFactory;
    private $dataPersistor;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        DataPersistorInterface $dataPersistor,
        QuestionFactory $questionFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->questionFactory = $questionFactory;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    public function execute()
    {

        if ($this->getRequest()->isPost()) {

            try {
                $question = $this->questionFactory->create();
                $question->setData($this->getRequest()->getParams())->save();
                $this->messageManager->addSuccessMessage(__("Question Saved Successfully."));
                $this->dataPersistor->clear('write_question');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e, __("We can\'t save your question, Please try again."));
                $this->dataPersistor->set('write_question', $this->getRequest()->getParams());
            }
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}