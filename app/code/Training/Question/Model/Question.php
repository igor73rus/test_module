<?php

namespace Training\Question\Model;

use Magento\Framework\Model\AbstractModel;
use Training\Question\Model\ResourceModel\Question as QuestionResource;

class Question extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(QuestionResource::class);
    }
}