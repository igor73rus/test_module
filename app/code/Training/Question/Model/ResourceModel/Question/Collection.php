<?php

namespace Training\Question\Model\ResourceModel\Question;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Training\Question\Model\Question;
use Training\Question\Model\ResourceModel\Question as QuestionResource;


class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Question::class, QuestionResource::class);
    }
}