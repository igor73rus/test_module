<?php

namespace Training\Question\Block;

use Magento\Framework\View\Element\BlockInterface;
use Magento\Framework\View\Element\Template;

class Question extends Template implements BlockInterface
{
    protected $questionFactory;

    public function __construct(
        Template\Context $context,
        \Training\Question\Model\ResourceModel\Question\CollectionFactory $questionCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->questionCollectionFactory = $questionCollectionFactory;
    }

    public function getQuestions()
    {
        return $this->questionCollectionFactory->create()->setOrder('store_id', 'desc');
    }
}