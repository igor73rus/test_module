<?php

namespace Training\Question\Block;

use Magento\Framework\View\Element\BlockInterface;
use Magento\Framework\View\Element\Template;

class Form extends Template implements BlockInterface
{
    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('question/question/formpost', ['_secure' => true]);
    }
}